package com.badlogicgames.superjumper;

/**
 * Created by gregori.valero on 05/04/2017.
 */

public class Heart {

    float stateTime;
    public float x;
    public float y;
    public Heart (float x, float y) {
        this.x=x;
        this.y=y;
        stateTime = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
