package com.badlogicgames.superjumper;

/**
 * Created by gregori.valero on 18/04/2017.
 */

public class PowerUp extends GameObject {
    public static final float PU_WIDTH = 0.1f;
    public static final float PU_HEIGHT = 0.1f;

    float stateTime;

    public PowerUp (float x, float y) {
        super(x, y, PU_WIDTH, PU_HEIGHT);
        stateTime = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}